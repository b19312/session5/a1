package com.zuitt.batch193;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args){

        Contact myFirstContact = new Contact("John Doe", "+63152468596", "Quezon City");
        System.out.println(myFirstContact.getName());

        System.out.println( myFirstContact.getName()+ " Has the following registered numbers: " + myFirstContact.getNumbers());

        System.out.println( myFirstContact.getName()+ " Has the following registered addresses: " + myFirstContact.getAddresses());

        Contact mySecondContact = new Contact();
        mySecondContact.setName("Jane Doe");
        mySecondContact.setNumbers("+63123456789");
        mySecondContact.setAddresses("Manila, Metro Manila");

        System.out.println(mySecondContact.getName());

        System.out.println( mySecondContact.getName()+ " Has the following registered numbers: " + mySecondContact.getNumbers());

        System.out.println( mySecondContact.getName()+ " Has the following registered addresses: " + mySecondContact.getAddresses());

        Phonebook myPhonebook = new Phonebook();
        myPhonebook.setContact(mySecondContact.getName());
        myPhonebook.setContact(myFirstContact.getName());
        System.out.println(myPhonebook.getContacts());



    }
}
