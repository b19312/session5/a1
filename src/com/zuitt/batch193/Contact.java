package com.zuitt.batch193;

import java.util.ArrayList;

public class Contact {

    public String name;

    public String numbers;

    public String addresses;

    //Constructors

    public Contact() {

    }

    public Contact(String name,String numbers, String addresses){
        this.name = name;
        this.numbers = numbers;
        this.addresses = addresses;
    }

    //Getters

    public String getName(){
        return this.name;
    }

    public String getNumbers(){
        return this.numbers;
    }

    public String getAddresses(){
        return this.addresses;
    }

    //Setters

    public void setName(String name){
        this.name = name;
    }

    public void setNumbers(String numbers){
        this.numbers = numbers;
    }

    public void setAddresses(String addresses){
        this.addresses = addresses;
    }


}
