package com.zuitt.batch193;

public class Phonebook {

    private String contacts;

    //constructor
    public Phonebook(){};

    public Phonebook(String contacts){
        this.contacts = contacts;
    }

    //getter
    public String getContacts(){
        return this.contacts;
    }

    //Setter
    public String setContact(String contacts){
        return this.contacts = contacts;
    }
}
